import { Document } from 'mongoose'
import { ObjectId } from 'mongodb';
export default interface IPerson extends Document {
    nombreCompleto: string;
    email: string;
    contraseña: string;
    telefono: string;
    rol: string;
}