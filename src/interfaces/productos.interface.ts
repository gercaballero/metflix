import { Document, ObjectId } from 'mongoose'

export default interface IProductos extends Document {
    nombre: string
    precio: string
    stock?: string
    estado?: string
    ventas_id?: [ObjectId]
}

export interface IUpdateProductos {
    nombre?: string
    precio?: string
    stock?: string
    estado?: string
    ventas_id?: [ObjectId]
}