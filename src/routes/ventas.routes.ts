import { Router } from 'express';
import * as ventasController from '../controllers/ventas.controller';
import { validateFields } from '../middlewares/validate-fields';
const router = Router();
const { body, validationResult } = require('express-validator');

router.post('/', ventasController.create);
router.get('/', ventasController.index);
router.get('/:id', ventasController.show);
router.put('/:id',[
    body("forma_de_pago","la forma de pago es incorrecta").isString().isIn(['contado','tarjeta']),
    validateFields
], ventasController.update);
router.delete('/:id', ventasController.destroy);

export default router;