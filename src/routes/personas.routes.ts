import { Router } from 'express';
import { body } from 'express-validator';
import * as personaController from '../controllers/personas.controller';
import { validateFields } from '../middlewares/validate-fields';
const router = Router();

router.post('/', [
    body("email","Debe ser un Email valido").isString().isEmail(),
    body("contraseña","La contraseña es obligatoria").isString(),
    body("telefono","El telefono es obligatorio").isNumeric(),
    body("rol","El rol debe ser admin/cliente").isString().isIn(['admin', 'cliente']),
    validateFields],personaController.store);
router.get('/', personaController.index);
router.get('/:id', personaController.show);
router.put('/:id', [
    body("nombreCompleto","El nombre es obligatorio").isString(),
    body("email","Debe ser un Email valido").isString().isEmail(),
    body("contraseña","La contraseña es obligatoria").isString(),
    body("telefono","El telefono es obligatorio").isNumeric(),
    body("rol","El rol debe ser admin/cliente").isString().isIn(['admin', 'cliente']),
    validateFields], personaController.update);
router.delete('/:id', personaController.destroy);

export default router;