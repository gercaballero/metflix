import { Request, Response } from "express";
import Persona from "../models/personas.model";
import IPerson from "../interfaces/personas.interface";
// Get all resources

export const index = async (req: Request, res: Response) => {
    // agregar filtros

    try {
        // const { ...data } = req.query; 
        // let filters = { ...data };
        
        // if (data.nombre) {
        //     filters = { ...filters, nombre: {$regex: data.nombre, $options: 'i'} }
        // }

        // let productos = await Producto.find(filters);
        let personas = await Persona.find();
        res.json(personas);
    } catch (error) {
        res.status(500).send('Algo salió mal');
    }
};

export const show = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        let persona = await Persona.findById(id);

        if (!persona)
            res.status(404).send(`No se encontró la persona con id: ${id}`);
        else
            res.json(persona);
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};

// Create a new resource
export const store = async (req: Request, res: Response) => {
    const { nombreCompleto, email, contraseña, telefono, rol } = req?.body

    if (
        !nombreCompleto ||
        !email ||
        !contraseña ||
        !rol
    ) return res.status(400).json({msg: `Parametros enviados incorrectos, debe enviar nombre, email, contraseña, telefono, rol: [admin, cliente]`})

    try {
        const newPersona = new Persona({
            nombreCompleto,
            email,
            contraseña,
            telefono,
            rol
        })
        await newPersona.save()
        return res.status(201).json({msg: 'Persona creada'})
    }
    catch {
        return res.status(500).json({msg: 'No se pudo crear Persona en la colección'})
    }
};


// Edit a resource
export const update = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    const { ...data } = req.body;
    try {
        let persona = await Persona.findById(id);

        if (!persona)
            return res.status(404).send(`No se encontró la persona con id: ${id}`);
        
        if (data.nombre) persona.nombreCompleto = data.nombre;
        if (data.email) persona.email = data.email
        if (data.contraseña) persona.contraseña = data.contraseña
        if (data.telefono) persona.telefono = data.telefono
        if (data.rol) persona.rol = data.rol

        await persona.save();
        
        res.status(200).json(persona);
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};

// Delete a resource
export const destroy = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        let persona = await Persona.findByIdAndDelete(id);
        console.log(persona);
        if (!persona)
            res.status(404).send(`No se encontró la persona con id: ${id}`);
        else
            res.status(200).json(persona);
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};