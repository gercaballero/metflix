import { model, Schema } from 'mongoose';
import IProduct from '../interfaces/productos.interface';
import IPerson from '../interfaces/personas.interface';
const PersonaSchema = new Schema({
    nombreCompleto: {
        type: String,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'El email es obligatorio']
    },
    contraseña: {
        type: String,
        
        required: [true, 'La contraseña es obligatoria']
    },
    telefono:{
        type: String,
        required: [true, 'El telefono es obligatorio']

    },
    rol: {
        type: String,
        required: [true, 'El rol es obligatorio. Valores posibles: admin/cliente'],
        enum: ['admin', 'cliente']
    }
}, {
    timestamps: { createdAt: true, updatedAt: true }
})

export default model<IPerson>('Person', PersonaSchema);